﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using Tema1Service.Protos;
namespace Tema1Utilizator
{
    class Program
    {

        static async Task Main(string[] args)
        {
            string cnp, nume;
            Console.WriteLine("Introduceti cnp-ul persoanei: \n");
            cnp = Console.ReadLine();
            Console.WriteLine("Introduceti numele persoanei: \n");
            nume = Console.ReadLine();
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Utilizator.UtilizatorClient(channel);
            try
            {
                var response = await client.IaInfoAsync(new UtilizatorRequest { Cnp = cnp, Nume = nume });
                Console.WriteLine(response);
            }
            catch(RpcException e)
            {
                Console.WriteLine(e.Status.Detail);
            }
        }

    }
}
