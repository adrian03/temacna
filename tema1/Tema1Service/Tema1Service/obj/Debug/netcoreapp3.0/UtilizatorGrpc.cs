// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: Protos/utilizator.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Tema1Service.Protos {
  public static partial class Utilizator
  {
    static readonly string __ServiceName = "Utilizator";

    static readonly grpc::Marshaller<global::Tema1Service.Protos.UtilizatorRequest> __Marshaller_UtilizatorRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Tema1Service.Protos.UtilizatorRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Tema1Service.Protos.UtilizatorReply> __Marshaller_UtilizatorReply = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Tema1Service.Protos.UtilizatorReply.Parser.ParseFrom);

    static readonly grpc::Method<global::Tema1Service.Protos.UtilizatorRequest, global::Tema1Service.Protos.UtilizatorReply> __Method_IaInfo = new grpc::Method<global::Tema1Service.Protos.UtilizatorRequest, global::Tema1Service.Protos.UtilizatorReply>(
        grpc::MethodType.Unary,
        __ServiceName,
        "IaInfo",
        __Marshaller_UtilizatorRequest,
        __Marshaller_UtilizatorReply);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Tema1Service.Protos.UtilizatorReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of Utilizator</summary>
    [grpc::BindServiceMethod(typeof(Utilizator), "BindService")]
    public abstract partial class UtilizatorBase
    {
      /// <summary>
      /// Sends a greeting
      /// </summary>
      /// <param name="request">The request received from the client.</param>
      /// <param name="context">The context of the server-side call handler being invoked.</param>
      /// <returns>The response to send back to the client (wrapped by a task).</returns>
      public virtual global::System.Threading.Tasks.Task<global::Tema1Service.Protos.UtilizatorReply> IaInfo(global::Tema1Service.Protos.UtilizatorRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(UtilizatorBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_IaInfo, serviceImpl.IaInfo).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, UtilizatorBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_IaInfo, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Tema1Service.Protos.UtilizatorRequest, global::Tema1Service.Protos.UtilizatorReply>(serviceImpl.IaInfo));
    }

  }
}
#endregion
