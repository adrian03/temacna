﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tema1Service.Protos;
using Tema1Service;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;

namespace Tema1Service.Services
{
    public class UtilizatorService : Utilizator.UtilizatorBase
    {
        const int NR_CNP = 13;
        private readonly ILogger<UtilizatorService> _logger;
        public UtilizatorService(ILogger<UtilizatorService> logger)
        {
            _logger = logger;
        }
        public override Task<UtilizatorReply> IaInfo(UtilizatorRequest UtilizatorRequest, ServerCallContext context)
        {
            string m_cnp;
            _logger.Log(LogLevel.Information, UtilizatorRequest.Cnp);
            _logger.Log(LogLevel.Information, UtilizatorRequest.Nume);
            m_cnp = UtilizatorRequest.Cnp;
           
            DateTime data_curenta = DateTime.Today;
           
            string anul_curent=data_curenta.ToString("yyyy");
            string luna_curenta=data_curenta.ToString("MM");
            string ziua_curenta=data_curenta.ToString("dd");
            string anul_din_cnp;
            if (m_cnp[0] == '1' || m_cnp[1] == '2')
            {
                 anul_din_cnp ="19"  + m_cnp[1].ToString() + m_cnp[2].ToString();
            }
            else
            {
                 anul_din_cnp = "20" + m_cnp[1].ToString() + m_cnp[2].ToString();
            }
            string luna_din_cnp = m_cnp[3].ToString() + m_cnp[4].ToString();
            string ziua_din_cnp = m_cnp[5].ToString() + m_cnp[6].ToString();
            int varsta = 0;
            if(NR_CNP!=m_cnp.Length)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "CNP-ul nu are 13 cifre."));
            }

            Regex regex = new Regex(@"^[a-zA-Z ,.'-]+$");
            if (!regex.IsMatch(UtilizatorRequest.Nume)) 
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Numele a fost introdus gresit"));

            if (String.Compare(luna_din_cnp, luna_curenta) < 0)
            {
                varsta = Convert.ToInt32(anul_curent) - Convert.ToInt32(anul_din_cnp);
            }
            else
            {
                if (String.Compare(luna_din_cnp, luna_curenta) > 0)
                {
                    varsta = Convert.ToInt32(anul_curent) - Convert.ToInt32(anul_din_cnp) - 1;
                }
                else
                {
                    if (String.Compare(luna_din_cnp, luna_curenta) == 0)
                    {
                        if (String.Compare(ziua_din_cnp, ziua_curenta) < 0)
                        {
                            varsta = Convert.ToInt32(anul_curent) - Convert.ToInt32(anul_din_cnp);
                        }
                        else
                        {
                            if (String.Compare(ziua_din_cnp, ziua_curenta) > 0)
                            {
                                varsta = Convert.ToInt32(anul_curent) - Convert.ToInt32(anul_din_cnp) - 1;
                            }
                            else
                            {
                                if (String.Compare(ziua_din_cnp, ziua_curenta) == 0)
                                {
                                    varsta = Convert.ToInt32(anul_curent) - Convert.ToInt32(anul_din_cnp);
                                }
                            }
                        }
                    }
                }
            }
           
            if (m_cnp[0] == '1' || m_cnp[0] == '3' || m_cnp[0] == '5' || m_cnp[0] == '7')
            {
                _logger.Log(LogLevel.Information, "\n" + "\n Nume: " + UtilizatorRequest.Nume + ".\n" +  " Sex: masculin." + "\n Varsta: " + varsta.ToString() + ".\n" + "\n");
            }
            else
            {
                if (m_cnp[0] == '2' || m_cnp[0] == '4' || m_cnp[0] == '6' || m_cnp[0] == '8')
                {
                    _logger.Log(LogLevel.Information, "\n" + "\n Nume: " + UtilizatorRequest.Nume + ".\n" + " Sex: feminin." + "\n Varsta: " + varsta.ToString() + ".\n" + "\n");
                }
                else
                {
                    throw new RpcException(new Status(StatusCode.InvalidArgument, "Sexul a fost introdus gresit"));
                }
            }
           
            _logger.Log(LogLevel.Information, varsta.ToString());
            return Task.FromResult(new UtilizatorReply { Message = "1" });
        }
    }
}